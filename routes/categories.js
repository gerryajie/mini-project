const express = require("express");
const router = express.Router();

//Import validator

//Import controller
const {
  getAllCategories,
  getDetailCategory,
} = require("../controllers/categories");

router.route("/").get(getAllCategories);

router.route("/:id").get(getDetailCategory);

module.exports = router;
