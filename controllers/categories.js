const { categories } = require("../models");

class Categories {
  static async getAllCategories(req, res, next) {
    try {
      let data = await categories.findAll({
        attributes: {
          exclude: ["createdAt", "updatedAt"],
        },
      });

      if (data.length === 0) {
        return res.status(404).json({ errors: ["Sorry, category not found."] });
      }

      res.status(200).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  static async getDetailCategory(req, res, next) {
    try {
      let data = await categories.findOne({ where: { id: req.params.id } });

      if (data.length === 0) {
        return res.status(404).json({ errors: ["Sorry, category not found"] });
      }

      res.status(200).json({ data });
    } catch (error) {
      res.status(500).json({ errors: "Internal Server Error" });
    }
  }
}

module.exports = new Categories();
